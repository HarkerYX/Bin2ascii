#ifndef __BIN_TO_ASCII_H
#define __BIN_TO_ASCII_H

#ifndef BOOL
	#define BOOL int
#endif

extern char *Bin2ascii(char *Buffer, int Size, BOOL Hex, BOOL Name, BOOL Ctrl, BOOL Escape);
extern char *Bin2hex(char *Buffer, int Size, char Separator);
extern char *CharType(int c);

#endif
